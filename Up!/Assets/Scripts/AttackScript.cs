﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackScript : MonoBehaviour {

   
    public Transform nozzle;
    public bool left;
    public GameObject gunfire;

    public float reloadTime=0.2f;
    public string targetLayer = "Player";
    private bool _enemyVisible = false;
    private float _timetoShoot = 0f;
    LayerMask layer;

    Rigidbody2D _rb;

    // Use this for initialization
    void Awake () {
        layer = LayerMask.GetMask(targetLayer);
        _rb = GetComponent<Rigidbody2D>();
        /*if(!left)
        {
            transform.localScale = new Vector3(-1f, 1f, 1f);
        }*/
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        if(Time.time>=_timetoShoot)
        {
            shoot();
        }
        

        RaycastHit2D hit = Physics2D.Raycast(nozzle.position, left ? Vector2.right : Vector2.left, 20f, layer);
        //Debug.DrawLine(nozzle.position, nozzle.position + (left ? Vector3.right : Vector3.left), Color.red);
        if (hit.collider != null)
        {
            //Debug.DrawLine(nozzle.position,hit.collider.transform.position, Color.red);
            if (hit.collider.CompareTag(targetLayer))
            {
                if (!_enemyVisible)
                {
                    _enemyVisible = true;
                    _timetoShoot = Time.time + reloadTime;
                }
            }
        }
	}

    void shoot()
    {
        if (_enemyVisible )
        {
            GameObject temp = Instantiate(gunfire, nozzle) as GameObject;
            temp.transform.localScale = new Vector3(-1f, left ? -1f : 1f, 1f);
            temp.GetComponent<GunfireScript>().left = left;
            temp.GetComponent<GunfireScript>().AimAndShoot();
            _enemyVisible = false;
        }
    }
}
