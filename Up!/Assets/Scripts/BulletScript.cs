﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour {

    public int damage = 1;
    public float speed = 10f;

    public bool left = false;
    public string targetLayer = "Player";

	// Use this for initialization
	void Awake () {
        //Debug.Log(" spawnBullet");
	}
	
	// Update is called once per frame
	void Update () {
        // dodać prędkość statku;
        transform.position += new Vector3((left ? 1f : -1f) * speed * Time.deltaTime, -PlayerScript.instance.speed*Time.deltaTime, 0f);
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag(targetLayer))
        {
            RaycastHit2D hit = Physics2D.Raycast(transform.position, left ? Vector2.left : Vector2.right);
            if (hit!=null)
            {
                collision.GetComponent<Health>().Damage(damage, hit.transform.position);
            }
            else
            {
                collision.GetComponent<Health>().Damage(damage, collision.transform.position);
            }
            Destroy(gameObject);
        }
    }


}
