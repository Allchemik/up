﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

abstract public class Health :MonoBehaviour  {

    public int Hp = 100;
    public GameObject particleToSpawn;

    public abstract void Damage(int dmg, Vector3 point);
}
