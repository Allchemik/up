﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RiverScript : MonoBehaviour {

    int width = 7;
    int height = 12;

    public GameObject WaterTile;

    public float movementSpeed = 1f;
    float riverLength = 1f;

	// Use this for initialization
	void Awake () {
        //WaterTile = Resources.Load<GameObject>("Prefabs/water_1.prefab")as GameObject;
        DrawRiver();
    }
	
	// Update is called once per frame
	void Update () {
        //movementSpeed = Mathf.Abs(movementSpeed) < PlayerScript.maxSpeed? PlayerScript.instance.speed:PlayerScript.maxSpeed;
        //movementSpeed += movementSpeed > 0 ? 0.1f : -0.1f;
        // float newPosition = Mathf.Repeat(Time.time * movementSpeed, riverLength);
        // transform.position = Vector3.down * newPosition;

        float move = transform.position.y - (Time.deltaTime * (movementSpeed + PlayerScript.instance.speed));
        float newPositionY = Mathf.Repeat(move, riverLength);
        transform.position = new Vector3(0f, newPositionY, 0f);

    }

    void DrawRiver()
    {
        //check length from start;

        for(int i=0;i<width;++i)
        {
            for(int j=0;j<height;++j)
            {
                GameObject temp = Instantiate(WaterTile, new Vector3(-3f + i, 6f - j, 0f), Quaternion.identity) as GameObject;
                temp.transform.parent = transform;
            }
        }
    }

}
