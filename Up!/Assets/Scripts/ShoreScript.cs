﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct LandElements
{
    public GameObject grass;
    public GameObject sand;
    public GameObject grassShore;
    public GameObject sandShore;
    public GameObject shore_grass2sand;
    public GameObject shore_sand2grass;
    public GameObject sand2grass;
    public GameObject grass2sand;
};
[System.Serializable]
public struct LandscapeElements
{
    public GameObject[] log;
    public GameObject[] cactus;
    public GameObject[] rock;
    public GameObject[] bush;
};
[System.Serializable]
public struct Foe
{
    public GameObject gunner;
    public GameObject Cannon;
};

public class ShoreScript : MonoBehaviour {

    public LandElements land;
    public LandscapeElements landscape;
    public Foe foe;

    public float movementSpeed;
    public bool left=true;

    private bool lastGrass=false;
    private float lastspawn = 0;


    // Use this for initialization
    void Start () {
        InitialDrawShore();

    }
	
	// Update is called once per frame
	void Update () {
        movementSpeed = PlayerScript.instance.speed;
        float newPosition = Time.deltaTime * movementSpeed;
        transform.position += Vector3.down * newPosition;
        if (movementSpeed > 0f)
        {
            if (transform.position.y < lastspawn)
            {
                lastspawn = transform.position.y - 1f;
                DrawLand(0f,true);
            }
        }
        else
        {
            if (transform.position.y > lastspawn)
            {
                lastspawn = transform.position.y + 1f;
                DrawLand(-12f,false);
            }
        }

    }

    void InitialDrawShore()
    {
        for (int i = 0; i < 12; ++i)
        {
            DrawLand(-i, false);
        }
    }

    void DrawLand(float offset,bool up)
    {
        bool grass = Random.Range(0f, 1f) > 0.5f; 
        GameObject temp;
        for (int j = 0; j < 3; ++j)
        {
            if (lastGrass == grass)
            {
                temp = Instantiate(grass ? land.grass : land.sand, transform) as GameObject;
            }
            else
            {
                temp = Instantiate((up?grass:!grass) ? land.sand2grass : land.grass2sand, transform) as GameObject;   
            }
            DrawEnvironment(grass, temp.transform);
            temp.transform.parent = transform;
            temp.transform.Translate(new Vector3((left?-3f:0f) + j, -transform.position.y + 6f + offset, 0f));
        }
        if (lastGrass == grass)
        {
            temp = Instantiate(grass ? land.grassShore : land.sandShore, transform) as GameObject;
        }
        else
        {
            temp = Instantiate((up ? grass : !grass) ? land.shore_sand2grass : land.shore_grass2sand, transform) as GameObject;
        }
        DrawEnvironment(grass, temp.transform);
        temp.transform.parent = transform;
        temp.transform.Translate(new Vector3((left?0f:-1f), -transform.position.y + 6f  + offset, 0f));

        lastGrass = grass;
    }

    void DrawEnvironment(bool grass,Transform tile)
    {
        bool spawnElement = Random.Range(0f, 1f) > 0.7f;

        if(spawnElement)
        {
            GameObject temp;
            int element;
            int elementType = Mathf.FloorToInt(Random.Range(0f, movementSpeed>0f?4f:2f));
            switch(elementType)
            {
                case 0:
                    element = Mathf.FloorToInt(Random.Range(0f, landscape.bush.Length));
                    temp = Instantiate(landscape.bush[element], tile) as GameObject;
                break;
                case 1:
                    element = Mathf.FloorToInt(Random.Range(0f, landscape.rock.Length));
                    temp = Instantiate(landscape.rock[element], tile) as GameObject;
                break;
                case 2:
                    if (grass == true && lastGrass == true)
                    {
                        element = Mathf.FloorToInt(Random.Range(0f, landscape.log.Length));
                        temp = Instantiate(landscape.log[element], tile) as GameObject;
                    }
                    else
                    {
                        element = Mathf.FloorToInt(Random.Range(0f, landscape.cactus.Length));
                        temp = Instantiate(landscape.cactus[element], tile) as GameObject;
                    }
                    break;
                default:
                    temp = DrawFoe(tile);
                break;
               }
            temp.transform.parent = tile;
        }
    }
    GameObject DrawFoe(Transform tile)
    {
        bool spawnCannon = Random.Range(0f, 1f) > 0.7f;
        GameObject temp;
        if (spawnCannon)
        {
            temp = Instantiate(foe.Cannon, tile) as GameObject;
            temp.transform.localScale = new Vector3(left ? -2 : 2f, 2f, 1f);
            temp.GetComponent<AttackScript>().left = left;
        }
        else
        {
            temp = Instantiate(foe.gunner, tile) as GameObject;
            temp.transform.localScale = new Vector3(left ? 2f : -2f, 2f, 1f);
            temp.GetComponent<AttackScript>().left = left;
        }

        return temp;
    }
}
