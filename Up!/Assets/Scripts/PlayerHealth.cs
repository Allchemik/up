﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealth : Health
{
    Rigidbody2D _rb;
    public Slider waterLevel;
    private bool kills;
    private bool distance;

    private void Awake()
    {
        _rb = GetComponent<Rigidbody2D>();
        waterLevel.value = 100 - Hp;
    }
    public override void Damage(int dmg, Vector3 point)
    {
        GameObject temp = Instantiate(particleToSpawn, point, Quaternion.identity) as GameObject;
        Destroy(temp, 1f);
        Hp -= dmg;
        _rb.mass = 1 + (50 * ((100-Hp)/100f));
        waterLevel.value = 100-Hp;
        if(Hp<=0)
        {
            _rb.mass = 100;

            if (PlayerPrefs.GetInt("Kills") < PlayerScript.instance.points)
            {
                kills = true;
                PlayerPrefs.SetInt("Kills", PlayerScript.instance.points);
                PlayerPrefs.Save();
            }
            if (PlayerPrefs.GetInt("Distance") < PlayerScript.instance.distance)
            {
                distance = true;
                PlayerPrefs.SetInt("Distance", Mathf.RoundToInt(PlayerScript.instance.distance));
                PlayerPrefs.Save();
            }
            SplashScreenManager.instance.showSpash(kills, distance);
        }
    }
}
