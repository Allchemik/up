﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunfireScript : MonoBehaviour {

    public GameObject bullet;
    public bool left;

    private void Awake()
    {
        Destroy(gameObject, 1f);
    }
    public void AimAndShoot()
    {
        StartCoroutine(shoot(left));
    }

    IEnumerator shoot(bool left)
    {
        yield return new WaitForSeconds(0.3f);
        GameObject temp = Instantiate(bullet, transform.position,Quaternion.identity) as GameObject;
        temp.GetComponent<BulletScript>().left = left;

        temp.transform.parent = null;
    }
}
