﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SplashScreenManager : MonoBehaviour {

    public static SplashScreenManager instance = null;

    public Image splashScreen;
    public Text GameOver;
    public Text Continue;
    public Text TopKills;
    public Text TopDistance;
    
	// Use this for initialization
	void Start () {
        if (instance == null)
            instance = this;
        if (instance != this)
            Destroy(gameObject);

        splashScreen.enabled = false;
        GameOver.enabled = false;
        TopKills.enabled = false;
        TopDistance.enabled = false;
        Continue.enabled = false;
    }
	
	public void showSpash(bool kills,bool distance)
    {
        splashScreen.enabled = true;
        GameOver.enabled = true;

        TopKills.text = "New Kill Highscore : " + PlayerPrefs.GetInt("Kills");
        TopKills.enabled = kills;
        TopDistance.text = "New Distance Highscore : " + PlayerPrefs.GetInt("Distance");
        TopDistance.enabled = kills;

        Continue.enabled = true;

    }
    public void Restart()
    {
        Application.LoadLevel(0);
    }
}
