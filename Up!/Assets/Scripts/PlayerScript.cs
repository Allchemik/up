﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerScript : MonoBehaviour {

    public static PlayerScript instance = null;
    Rigidbody2D _rb;

    public float distance = 0;
    public Text distanceText;
    public int points = 0;
    public Text pointsText;


    public float speed = 1f;
    public static float maxSpeed = 1.5f;

    public GameObject Turbine;


	// Use this for initialization
	void Awake () {
        if(instance==null)
        {
            instance = this;
        }
        else if(instance != this)
        {
            Destroy(gameObject);
        }
        _rb = GetComponent<Rigidbody2D>();
        Turbine.SetActive(false);
    }

    private void FixedUpdate()
    {
        //_rb.velocity = new Vector2(0f, speed);
        if (Input.GetButton("Jump"))
        {
            _rb.AddForce(new Vector2(0f, 50f));
            Turbine.SetActive(true);
        }
        else
        {
            Turbine.SetActive(false);
        }
        if (_rb.velocity.magnitude > maxSpeed)
        {
            speed = _rb.velocity.normalized.y * maxSpeed;
            _rb.velocity = new Vector2(0f, speed);
        }
    }


    void Update () {
        speed = _rb.velocity.y;
        transform.position = Vector3.zero;
        distance += speed * Time.deltaTime;
        distanceText.text = Mathf.RoundToInt(distance).ToString();
        pointsText.text = points.ToString();
        /*if (Mathf.Abs(_rb.velocity.magnitude) > maxSpeed)
        {
            speed = _rb.velocity.normalized.y * maxSpeed;
            _rb.velocity = Vector2.zero;
        }
        else
        {
            speed = _rb.velocity.magnitude;
            _rb.velocity = Vector2.zero;
        }*/
    }
}
