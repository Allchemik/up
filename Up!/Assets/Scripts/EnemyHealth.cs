﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : Health {

    public Sprite dead;

    public override void Damage(int dmg, Vector3 point)
    {
        Hp -= dmg;
        if(Hp<=0)
        {
            GetComponent<SpriteRenderer>().sprite = dead;
            GetComponent<AttackScript>().enabled = false;
            gameObject.layer = 2;
            gameObject.tag = "Untagged";
            PlayerScript.instance.points++;
        }
        GameObject temp = Instantiate(particleToSpawn, point, Quaternion.identity) as GameObject;
        temp.transform.parent = transform;
        Destroy(temp, 1f);
    }
}
